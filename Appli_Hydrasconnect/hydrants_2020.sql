-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 09 Janvier 2020 à 16:25
-- Version du serveur: 5.5.28-log
-- Version de PHP: 5.4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `hydrants_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `commune`
--

CREATE TABLE IF NOT EXISTS `commune` (
  `idCommune` int(11) NOT NULL AUTO_INCREMENT,
  `nomCommune` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  PRIMARY KEY (`idCommune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `donnee`
--

CREATE TABLE IF NOT EXISTS `donnee` (
  `idData` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typeMess` enum('ouverture','fermeture','renversement','activation','veille') COLLATE utf8_unicode_ci NOT NULL,
  `angle` float NOT NULL,
  `temperature` float NOT NULL,
  `volume` float NOT NULL,
  `duree` int(11) NOT NULL,
  `idHydrant` int(11) NOT NULL,
  PRIMARY KEY (`idData`),
  KEY `idHydrant` (`idHydrant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='TABLE DE STOCKAGE DES DONNES DES HYDRANTS' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `hydrant`
--

CREATE TABLE IF NOT EXISTS `hydrant` (
  `idHydrant` int(11) NOT NULL AUTO_INCREMENT,
  `codeSigfox` int(11) NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lat` float NOT NULL,
  `long` float NOT NULL,
  `debitNominal` float NOT NULL,
  `actifVeille` tinyint(1) NOT NULL DEFAULT '0',
  `renverse` tinyint(1) NOT NULL DEFAULT '0',
  `installOK` tinyint(1) NOT NULL DEFAULT '0',
  `dateFirstActivation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idCommune` int(11) NOT NULL,
  PRIMARY KEY (`idHydrant`),
  KEY `idCommune` (`idCommune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='TABLE DE STOCKAGE DES HYDRANTS DANS CHAQUE COMMUNE' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `idContact` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` enum('maire','employé','technicien') COLLATE utf8_unicode_ci NOT NULL,
  `telAlerte` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typeAlerte` enum('aucune','sms','mail','smsmail') COLLATE utf8_unicode_ci NOT NULL,
  `idCommune` int(11) NOT NULL,
  PRIMARY KEY (`idContact`),
  KEY `idCommune` (`idCommune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `donnee`
--
ALTER TABLE `donnee`
  ADD CONSTRAINT `donnee_ibfk_1` FOREIGN KEY (`idHydrant`) REFERENCES `hydrant` (`idHydrant`);

--
-- Contraintes pour la table `hydrant`
--
ALTER TABLE `hydrant`
  ADD CONSTRAINT `hydrant_ibfk_1` FOREIGN KEY (`idCommune`) REFERENCES `commune` (`idCommune`);

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `personne_ibfk_1` FOREIGN KEY (`idCommune`) REFERENCES `commune` (`idCommune`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
