var urlAuth = "https://hydrasconnect.com";
var userData = null ;
var listHydrantUser;
var infoCommune ;
var carteLeaflet = null;
var marker_city = null;

$(function () {

    $("body>[data-role='panel']").panel();

    jQuery(function () {

        $('#nav_panel').enhanceWithin().panel();

    });

    $(":mobile-pagecontainer").on("pagecontainerchange", function (e, ui){
        
        //Si on se trouve la page p_accueil

        if(ui.toPage[0].id === "p_accueil"){
            
            //Si tu l'utilisateur n'est pas connecté

            if(userData === null)
            {
                
                //On réinitalise les zones de saisies (Pour Password)

                $("#Password").keyup(function(){

                    resetInput() ;

                });
                
                //On réinitalise les zones de saisies (Pour Email)

                $("#Email").keyup(function(){

                    resetInput() ;

                });
                
                //Si on execute le formulaire

                $("#formLogin").submit(function (event) {
                    
                    //On execute la fonction login

                    login(event);

                });
                
            //Si tu l'utilisateur est connecté alors on le redirige vers p_gestion

            } else {

                $(':mobile-pagecontainer').pagecontainer(
                    'change',
                    '#p_gestion',
                    {transition: 'none'}
                );
            }

        }
        
        //Si on se trouve la page p_gestion

        if(ui.toPage[0].id === "p_gestion")
        {

            if((userData !== null) && (userData.isSuccess))
            {
                //On execute la fonction GetDataHydrant (on passe en paramètre l'id de la ville de l'utilisateur)
                
                GetDataHydrant(userData.idCommune);
                
                //Lorsque l'utlisateur appuie sur le bouton user-disconnect

                $("#user-disconnect").click(function(){

                    disconnect();
    
                });
                
            }

        }

    });

});

function GetDataHydrant(prmDataCommune){  
    
    $("#compt-data").html("<img src='img/loading.gif' alt='loading' class='img-loading' style='margin:auto, display:block'");
    
    $.ajax({
        type: "GET",
        url: urlAuth + "/REST/Hydrant/index/" + prmDataCommune + "/data",
        dataType: 'json',
        json: "json",
        success: onGetSuccessData,
        error: onGetErrorData
    })

    function onGetSuccessData(reponse, status){
        
        $.ajax({
            type: "GET",
            url: urlAuth + "/REST/Hydrant/index/" + prmDataCommune,
            dataType: 'json',
            json: "json",
            success: function(value, status)
            {
                
               infoCommune = value[0] ;
                
            }
        }); 

        
        //console.log(reponse) ;
        
        //On réinitialise les marker sur carte
        
        if(carteLeaflet !== null)
        {
            carteLeaflet.removeLayer(marker_city)
        
            marker_city = null ; 
        }

        $("#ListHydrant").html("");
        
        //On recopie la réponse de la réquête AJAX dans une variable globale
         
        listHydrantUser = reponse ; 

        var cpt = 0 ;
        
        //Boucle pour afficher la liste des hydrants dans la commune à installer
        
        for (var i = 0; i < listHydrantUser.length; i++) {

            if(listHydrantUser[i].codeSigfox === null && listHydrantUser[i].dateFirstActivation === "")
            {
              
                $("#content-user-data ol").append("<li><a href='#' class='element' data-index='"+ i +"' style='color:#000'>"+ listHydrantUser[i].adresse +"</a></li>");

                cpt++;
                
            }
           
        }
        
        $("#ListHydrant").listview("refresh");
          
        var nbHydrantInstall = cpt ;

        if(nbHydrantInstall === 0)
        {
            nbHydrantInstall = "aucun";
        }

        //Affichage du nombre d'hydrants à installer 
        
        $("#compt-data").html("(Il y a " + nbHydrantInstall + " hydrants à installer)") ;
        
        //Lorsque l'on clique sur un élément de la liste (sur un hydrant)
        
        $(".element").click(function(){
            
            HydrantSelect = $(this).data("index"); 
            
            //console.log(HydrantSelect);
            
            $(':mobile-pagecontainer').pagecontainer(
                    'change',
                    '#p_hydrant_select',
                    {transition: 'none'}
            );
            
            $("#hydrant-data").html("") ;
            
            //On récupère les information uniquement pour l'hydrant selectionné
            
            resultHydrant = listHydrantUser[HydrantSelect] ;
            
            //console.log(resultHydrant) ;
            
            $("#hydrant-data").html("<h3>" + resultHydrant.adresse + "<br> " + infoCommune.nomCommune + " </h3>") ;
            
            $("#hydrant-data").append("<br>" +
                                      "<input id='active' class='' type='button' value='Activation' />"
                                     ) ;
            
            $('#active').click(function(){
                
                var idSigFox = prompt('Entrez l\'identifiant SigFox :') ;
                
                //console.log(idSigFox) ;
                
                activation(idSigFox) ;
                
                /*On vérifie si l'identifiant données est déjà présent dans la base de données*/
                
                
                /*Sinon on le met à jour dans la BDD (L'hydrant prend comme identifiant SigFox ce qu'à rentrer l'utilisateur) */
                
            });
            
            //Affichage de la carte 
            
            if (carteLeaflet === null)
            {
                
                carteLeaflet = new L.Map("mapid", {
                    center: [resultHydrant.lat, resultHydrant.long],
                    zoom: 16
                });
            
                new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(carteLeaflet);     
                    
            }
            
            marker_city = L.marker([resultHydrant.lat, resultHydrant.long]).addTo(carteLeaflet);
            
            carteLeaflet.setView(new L.LatLng(resultHydrant.lat, resultHydrant.long), 16) ;
            
        });

        $("#user-disconnect").click(function(){

            disconnect();

        });
       
    }
    
    function onGetErrorData(status){
        
        //console.log(JSON.stringify(status)) ;
        
    }  
    
}  

function resetInput() 
{

    $(".ui-input-text").removeClass('alert-error');

    $(".ui-input-text").removeClass('alert-error');

} 

function disconnect()
{
    userData = null ;

    $("#user-disconnect").remove();

    $("#ListHydrant").listview("refresh");

    $("#ListHydrant").html("");

    $("#compt-data").html('Veuillez vous connecter pour accèder aux fonctionnalités de l\'application<br><br>'+
                          '<a href="#p_accueil" class="item-nav-content" data-transition="none">Connexion</a>');

    $(':mobile-pagecontainer').pagecontainer(
        'change',
        '#p_accueil',
        {transition: 'none'}
    );
}

function login(event)
{
    event.preventDefault();

    var postdata = $('#formLogin').serialize();

    $.ajax({
        type: "POST",
        url: urlAuth + "/REST/Log/authentificationApp/",
        dataType: 'json',
        json: "json",
        data: postdata,
        success: onGetSuccess,
        error: onGetError
    });

    function onGetSuccess(reponse, status){
        
        //console.log(reponse) ;
                        
        if (reponse.isSuccess) {

            userData = reponse ;

            $("#disconnect").html("<a href='#' class='discconect-btn' id='user-disconnect'>Déconnexion</a>") ; 

            $("#echec-login").html("") ;

            $(':mobile-pagecontainer').pagecontainer(
                'change',
                '#p_gestion',
                {transition: 'none'}
            );

        } else {

            $("#echec-login").html("<div class='alert-login'>" + reponse.error + "</div>") ;
                            
            $(".ui-input-text").addClass('alert-error') ;
                        
            $(".ui-input-text").addClass('alert-error') ;

        }

    }

    function onGetError(status){

       $('#echec-login').html('<div class="alert-login">Erreur : Le serveur rencontre des problèmes désolé </div>');
                
    }
}


function activation(prmSigFox)
{
    if(prmSigFox !== "")
    {
                    
        $.ajax({
            type: "POST",
            url: urlAuth + "/REST/Hydrant/sigfox",  
            dataType: 'json',
            json: "json",
            data: 'dto=' + prmSigFox,
            success: onGetSuccessSigfox, 
            error: onGetErrorSigfox
        }); 
                    
        function onGetSuccessSigfox(reponse, status)
        {

            console.log(reponse) ;

        }
                    
        function onGetErrorSigfox(status)
        {

            console.log(status) ;

        }
                   
                    
    } else {
                    
        confirm("Veuillez indiquer un identifiant pour cette hydrant.") ;
                    
    }
    
}